package ro.tuc.heroku.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.heroku.HerokuApplicationConfig;
import ro.tuc.heroku.dtos.PersonDTO;
import ro.tuc.heroku.dtos.components.common.HumanName;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class PersonServiceIntegrationTests extends HerokuApplicationConfig {

    @Autowired
    PersonService personService;

    @Test
    public void testGetCorrect() {
        List<PersonDTO> personDTOList = personService.findPersons();
        assertEquals("Test Insert Person", 1, personDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        PersonDTO p = PersonDTO
                .builder()
                .name(new ArrayList<HumanName>(){
                    {
                        add(HumanName.builder()
                                .use("official")
                                .family("NoName")
                                .given(new ArrayList<String>(){
                                    {
                                        add("Maria");
                                        add("Larisa");
                                    }
                                })
                                .build());
                    }
                })
                .build();
        String insertedID = personService.insert(p);

        PersonDTO insertedPerson = PersonDTO.builder()
                .id(insertedID)
                .name(p.getName())
                .build();
        PersonDTO fetchedPerson = personService.findPersonById(insertedID);

        assertEquals("Test Inserted Person", insertedPerson.getId(), fetchedPerson.getId());
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        PersonDTO p = PersonDTO
                .builder()
                .id(UUID.randomUUID().toString())
                .name(new ArrayList<HumanName>(){
                    {
                        add(HumanName.builder()
                                .use("official")
                                .family("NoName")
                                .given(new ArrayList<String>(){
                                    {
                                        add("Maria");
                                        add("Larisa");
                                    }
                                })
                                .build());
                    }
                })
                .build();
        personService.insert(p);

        List<PersonDTO> personDTOList = personService.findPersons();
        assertEquals("Test Inserted Persons", 2, personDTOList.size());
    }
}
