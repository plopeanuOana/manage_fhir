package ro.tuc.heroku.controllers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.r4.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.heroku.dtos.OrganizationDTO;
import ro.tuc.heroku.dtos.builders.OrganizationBuilder;
import ro.tuc.heroku.services.OrganizationService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/organization")
public class OrganizationController {

    private final OrganizationService organizationService;

    @Autowired
    public OrganizationController(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @GetMapping()
    public ResponseEntity<List<OrganizationDTO>> getOrganizations() {
        List<OrganizationDTO> dtos = organizationService.findOrganizations();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/text")
    public ResponseEntity<String> insertString(@Valid @RequestBody String data) {
        FhirContext ctx = FhirContext.forR4();
        IParser parser = ctx.newJsonParser();
        Organization organizaiton = parser.parseResource(Organization.class, data);

        OrganizationDTO organizationDTO = OrganizationBuilder.fhirToDTO(organizaiton);
        String organizationId = organizationService.insert(organizationDTO);
        return new ResponseEntity<>(organizationId, HttpStatus.CREATED);
    }

    @PostMapping()
    public ResponseEntity<String> insert(@Valid @RequestBody OrganizationDTO organizationDTO) {
        String organizationId = organizationService.insert(organizationDTO);
        return new ResponseEntity<>(organizationId, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<OrganizationDTO> getOrganization(@PathVariable("id") String organizationId) {
        OrganizationDTO dto = organizationService.findOrganizationById(organizationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public HttpStatus deleteOrganization(@PathVariable("id") String organizationId) {
        organizationService.delete(organizationId);
        return HttpStatus.OK;
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<String> updateOrganization(@PathVariable("id") String id, @Valid @RequestBody OrganizationDTO organizationDTO) {
        String dtoId = organizationService.update(id, organizationDTO);
        return new ResponseEntity<>(dtoId, HttpStatus.OK);
    }
}


