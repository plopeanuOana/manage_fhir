package ro.tuc.heroku.controllers;


import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.r4.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.heroku.dtos.PersonDTO;
import ro.tuc.heroku.dtos.builders.PersonBuilder;
import ro.tuc.heroku.services.PersonService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping()
    public ResponseEntity<List<PersonDTO>> getPersons() {
        List<PersonDTO> dtos = personService.findPersons();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(value = "/text")
    public ResponseEntity<String> insertString(@Valid @RequestBody String data) {
        FhirContext ctx = FhirContext.forR4();
        IParser parser = ctx.newJsonParser();
        Person parsed = parser.parseResource(Person.class, data);

        PersonDTO personDTO = PersonBuilder.fhirToDTO(parsed);
        String personID = personService.insert(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @PostMapping()
    public ResponseEntity<String> insert(@Valid @RequestBody PersonDTO personDTO) {
        String personID = personService.insert(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PersonDTO> getPerson(@PathVariable("id") String personId) {
        PersonDTO dto = personService.findPersonById(personId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public HttpStatus deletePerson(@PathVariable("id") String personId) {
        personService.delete(personId);
        return HttpStatus.OK;
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<String> updatePerson(@PathVariable("id") String id, @Valid @RequestBody PersonDTO personDTO) {
        String dtoId = personService.update(id, personDTO);
        return new ResponseEntity<>(dtoId, HttpStatus.OK);
    }
}
