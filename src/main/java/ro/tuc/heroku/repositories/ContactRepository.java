package ro.tuc.heroku.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.tuc.heroku.entities.components.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, String> {
}
