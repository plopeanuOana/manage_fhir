package ro.tuc.heroku.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.tuc.heroku.entities.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, String> {
}
