package ro.tuc.heroku.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.tuc.heroku.entities.Organization;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, String> {
}
