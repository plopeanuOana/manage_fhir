package ro.tuc.heroku.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import ro.tuc.heroku.entities.components.Contact;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class Organization {
    @NotNull
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @Column(columnDefinition = "text")
    private String identifier;

    private Boolean active;

    @Column(columnDefinition = "text")
    private String type;

    private String name;

    @Column(columnDefinition = "text")
    private String alias;

    @Column(columnDefinition = "text")
    private String telecom;

    @Column(columnDefinition = "text")
    private String address;

    private String partOf;

    @OneToMany(mappedBy = "organization", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Contact> contact;

    private String endpoint;
}
