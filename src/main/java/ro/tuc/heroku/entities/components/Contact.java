package ro.tuc.heroku.entities.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import ro.tuc.heroku.entities.Organization;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class Contact {
    @NotNull
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    @Column(columnDefinition = "text")
    private String purpose;
    @Column(columnDefinition = "text")
    private String name;
    @Column(columnDefinition = "text")
    private String telecom;
    @Column(columnDefinition = "text")
    private String address;
    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;
}
