package ro.tuc.heroku.entities.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import ro.tuc.heroku.entities.Person;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class Link {
    @NotNull
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    @Column(columnDefinition = "text")
    private String target;
    @Column(columnDefinition = "text")
    private String assurance;
    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;
}
