package ro.tuc.heroku.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.heroku.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.heroku.dtos.PersonDTO;
import ro.tuc.heroku.dtos.builders.PersonBuilder;
import ro.tuc.heroku.dtos.builders.components.LinkBuilder;
import ro.tuc.heroku.entities.Person;
import ro.tuc.heroku.repositories.LinkRepository;
import ro.tuc.heroku.repositories.PersonRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PersonRepository personRepository;
    private final LinkRepository linkRepository;

    @Autowired
    public PersonService(PersonRepository personRepository, LinkRepository linkRepository) {
        this.personRepository = personRepository;
        this.linkRepository = linkRepository;
    }

    public List<PersonDTO> findPersons() {
        List<Person> personList = personRepository.findAll();
        return personList.stream()
                .map(PersonBuilder::entityToPersonDTO)
                .collect(Collectors.toList());
    }

    public PersonDTO findPersonById(String id) {
        return PersonBuilder.entityToPersonDTO(getPerson(id));
    }

    public String insert(PersonDTO personDTO) {
        Person person = savePerson(personDTO);
        LOGGER.debug("Person with id {} was inserted in db", person.getId());
        return person.getId();
    }

    public void delete(String id) {
        getPerson(id);
        personRepository.deleteById(id);
    }

    public String update(String id, PersonDTO personDTO) {
        getPerson(id);
        Person person = savePerson(personDTO);
        LOGGER.debug("Person with id {} was updated in db", person.getId());
        return person.getId();
    }

    private Person savePerson(PersonDTO personDTO) {
        Person person = PersonBuilder.toEntity(personDTO);
        person = personRepository.save(person);

        if (personDTO.getLink() != null && personDTO.getLink().size() > 0) {
            Person finalPerson = person;
            personDTO.setLink(
                    personDTO.getLink()
                            .stream()
                            .map((link) -> LinkBuilder.setPersonDTO(link, finalPerson.getId()))
                            .collect(Collectors.toList()));
            linkRepository.saveAll(personDTO.getLink()
                    .stream()
                    .map(LinkBuilder::toEntity)
                    .collect(Collectors.toList()));
        }
        return person;
    }

    private Person getPerson(String id) {
        Optional<Person> personOptional = personRepository.findById(id);
        if (!personOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return personOptional.get();
    }
}
