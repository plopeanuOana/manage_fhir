package ro.tuc.heroku.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.heroku.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.heroku.dtos.OrganizationDTO;
import ro.tuc.heroku.dtos.builders.OrganizationBuilder;
import ro.tuc.heroku.dtos.builders.components.ContactBuilder;
import ro.tuc.heroku.entities.Organization;
import ro.tuc.heroku.repositories.ContactRepository;
import ro.tuc.heroku.repositories.OrganizationRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrganizationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final OrganizationRepository organizationRepository;
    private final ContactRepository contactRepository;

    @Autowired
    public OrganizationService(OrganizationRepository organizationRepository, ContactRepository contactRepository) {
        this.organizationRepository = organizationRepository;
        this.contactRepository = contactRepository;
    }

    public List<OrganizationDTO> findOrganizations() {
        List<Organization> organizationList = organizationRepository.findAll();
        return organizationList.stream()
                .map(OrganizationBuilder::entityToOrganizationDTO)
                .collect(Collectors.toList());
    }

    public OrganizationDTO findOrganizationById(String id) {
        return OrganizationBuilder.entityToOrganizationDTO(getOrganization(id));
    }

    public String insert(OrganizationDTO organizationDTO) {
        Organization organization = saveOrganization(organizationDTO);
        LOGGER.debug("Organization with id {} was inserted in db", organization.getId());
        return organization.getId();
    }

    public void delete(String id) {
        getOrganization(id);
        organizationRepository.deleteById(id);
    }

    public String update(String id, OrganizationDTO organizationDTO) {
        getOrganization(id);
        Organization organization = saveOrganization(organizationDTO);
        LOGGER.debug("Person with id {} was updated in db", organization.getId());
        return organization.getId();
    }

    private Organization getOrganization(String id) {
        Optional<Organization> organizationOptional = organizationRepository.findById(id);
        if (!organizationOptional.isPresent()) {
            LOGGER.error("Organization with id {} was not found in db", id);
            throw new ResourceNotFoundException(Organization.class.getSimpleName() + " with id: " + id);
        }
        return organizationOptional.get();
    }

    private Organization saveOrganization(OrganizationDTO organizationDTO) {
        Organization organization = OrganizationBuilder.toEntity(organizationDTO);
        organization = organizationRepository.save(organization);

        if (organizationDTO.getContact() != null && organizationDTO.getContact().size() > 0) {
            Organization finalOrganization = organization;
            organizationDTO.setContact(
                    organizationDTO.getContact()
                            .stream()
                            .map((contact) -> ContactBuilder.setOrganizationDTO(contact, finalOrganization.getId()))
                            .collect(Collectors.toList()));

            contactRepository.saveAll(organizationDTO.getContact()
                    .stream()
                    .map(ContactBuilder::toEntity)
                    .collect(Collectors.toList()));
        }
        return organization;
    }
}
