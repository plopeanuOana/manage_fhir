package ro.tuc.heroku.dtos;

import lombok.*;
import ro.tuc.heroku.dtos.components.LinkDTO;
import ro.tuc.heroku.dtos.components.common.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class PersonDTO {
    private String id;
    private List<Identifier> identifier;
    private List<HumanName> name;
    private List<ContactPoint> telecom;
    private String gender;
    private String birthDate;
    private List<Address> address;
    private Attachment photo;
    private Reference managingOrganization;
    private Boolean active;
    private List<LinkDTO> link;
}

