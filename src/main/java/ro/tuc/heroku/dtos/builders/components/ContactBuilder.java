package ro.tuc.heroku.dtos.builders.components;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.experimental.UtilityClass;
import org.hl7.fhir.r4.model.Organization;
import ro.tuc.heroku.dtos.OrganizationDTO;
import ro.tuc.heroku.dtos.builders.UtilBuilder;
import ro.tuc.heroku.dtos.components.ContactDTO;
import ro.tuc.heroku.dtos.components.common.Address;
import ro.tuc.heroku.dtos.components.common.CodeableConcept;
import ro.tuc.heroku.dtos.components.common.ContactPoint;
import ro.tuc.heroku.dtos.components.common.HumanName;
import ro.tuc.heroku.entities.components.Contact;

import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class ContactBuilder {

    public ContactDTO fhirToContactDTO(Organization.OrganizationContactComponent contact, String organization_id) {
        return ContactDTO
                .builder()
                .purpose(contact.getPurpose() == null ? null :
                        UtilBuilder.toCodeableConcept(contact.getPurpose()))
                .name(contact.getName() == null ? null :
                        UtilBuilder.toHumanName(contact.getName()))
                .telecom(contact.getTelecom() == null ? null :
                        contact.getTelecom()
                                .stream()
                                .map(UtilBuilder::toContactPoint)
                                .collect(Collectors.toList()))
                .address(contact.getAddress() == null ? null :
                        UtilBuilder.toAddress(contact.getAddress()))
                .organizationDTO(OrganizationDTO.builder().id(organization_id).build())
                .build();
    }

    public ContactDTO setOrganizationDTO(ContactDTO contactDTO, String organization_id) {
        return ContactDTO.builder()
                .purpose(contactDTO.getPurpose())
                .name(contactDTO.getName())
                .telecom(contactDTO.getTelecom())
                .address(contactDTO.getAddress())
                .organizationDTO(OrganizationDTO.builder().id(organization_id).build())
                .build();
    }

    public Contact toEntity(ContactDTO contactDTO) {
        Gson gson = new Gson();
        return Contact
                .builder()
                .id(contactDTO.getId())
                .purpose(gson.toJson(contactDTO.getPurpose()))
                .name(gson.toJson(contactDTO.getName()))
                .telecom(gson.toJson(contactDTO.getTelecom()))
                .address(gson.toJson(contactDTO.getAddress()))
                .organization(ro.tuc.heroku.entities.Organization.builder().id(contactDTO.getOrganizationDTO().getId()).build())
                .build();
    }

    public ContactDTO entityToContactDTO(Contact contact) {
        Gson gson = new Gson();
        return ContactDTO
                .builder()
                .id(contact.getId())
                .purpose(gson.fromJson(contact.getPurpose(), CodeableConcept.class))
                .name(gson.fromJson(contact.getName(), HumanName.class))
                .telecom(gson.fromJson(contact.getTelecom(), new TypeToken<List<ContactPoint>>() {
                }.getType()))
                .address(gson.fromJson(contact.getAddress(), Address.class))
                .organizationDTO(OrganizationDTO.builder().id(contact.getId()).build())
                .build();
    }
}
