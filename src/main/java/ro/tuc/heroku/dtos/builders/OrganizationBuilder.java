package ro.tuc.heroku.dtos.builders;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.experimental.UtilityClass;
import org.hl7.fhir.r4.model.PrimitiveType;
import ro.tuc.heroku.dtos.OrganizationDTO;
import ro.tuc.heroku.dtos.builders.components.ContactBuilder;
import ro.tuc.heroku.dtos.components.common.Address;
import ro.tuc.heroku.dtos.components.common.ContactPoint;
import ro.tuc.heroku.dtos.components.common.Identifier;
import ro.tuc.heroku.dtos.components.common.Reference;
import ro.tuc.heroku.entities.Organization;

import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class OrganizationBuilder {

    public OrganizationDTO fhirToDTO(org.hl7.fhir.r4.model.Organization organization) {
        return OrganizationDTO
                .builder()
                .id(organization.getId())
                .identifier(organization.getIdentifier()
                        .stream()
                        .map(UtilBuilder::toIdentifier)
                        .collect(Collectors.toList()))
                .active(organization.getActive())
                .type(organization.getType()
                        .stream()
                        .map(UtilBuilder::toCodeableConcept)
                        .collect(Collectors.toList()))
                .name(organization.getName())
                .alias(organization.getAlias()
                        .stream()
                        .map(PrimitiveType::getValue)
                        .collect(Collectors.toList()))
                .telecom(organization.getTelecom()
                        .stream()
                        .map(UtilBuilder::toContactPoint)
                        .collect(Collectors.toList()))
                .address(organization.getAddress()
                        .stream()
                        .map(UtilBuilder::toAddress)
                        .collect(Collectors.toList()))
                .partOf(UtilBuilder.toReference(organization.getPartOf()))
                .contact(organization.getContact()
                        .stream()
                        .map((contact) ->
                                ContactBuilder.fhirToContactDTO(contact, organization.getId()))
                        .collect(Collectors.toList()))
                .endpoint(organization.getEndpoint()
                        .stream()
                        .map(UtilBuilder::toReference)
                        .collect(Collectors.toList()))
                .build();
    }

    public OrganizationDTO entityToOrganizationDTO(Organization organization) {
        Gson gson = new Gson();
        return OrganizationDTO
                .builder()
                .id(organization.getId())
                .identifier(gson.fromJson(organization.getIdentifier(), new TypeToken<List<Identifier>>() {
                }.getType()))
                .active(organization.getActive())
                .type(gson.fromJson(organization.getType(), new TypeToken<List<Identifier>>() {
                }.getType()))
                .name(organization.getName())
                .alias(gson.fromJson(organization.getAlias(), new TypeToken<List<String>>() {
                }.getType()))
                .telecom(gson.fromJson(organization.getTelecom(), new TypeToken<List<ContactPoint>>() {
                }.getType()))
                .address(gson.fromJson(organization.getAddress(), new TypeToken<List<Address>>() {
                }.getType()))
                .partOf(gson.fromJson(organization.getPartOf(), Reference.class))
                .contact(organization.getContact() == null ? null :
                        organization.getContact()
                                .stream()
                                .map(ContactBuilder::entityToContactDTO)
                                .collect(Collectors.toList()))
                .endpoint(gson.fromJson(organization.getEndpoint(), new TypeToken<List<Reference>>() {
                }.getType()))
                .build();
    }

    public Organization toEntity(OrganizationDTO organizationDTO) {
        Gson gson = new Gson();
        return Organization.builder()
                .id(organizationDTO.getId())
                .identifier(gson.toJson(organizationDTO.getIdentifier()))
                .active(organizationDTO.getActive())
                .type(gson.toJson(organizationDTO.getType()))
                .name(organizationDTO.getName())
                .alias(gson.toJson(organizationDTO.getAlias()))
                .telecom(gson.toJson(organizationDTO.getTelecom()))
                .address(gson.toJson(organizationDTO.getAddress()))
                .partOf(gson.toJson(organizationDTO.getPartOf()))
                //contact
                .build();
    }
}
