package ro.tuc.heroku.dtos.builders;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.experimental.UtilityClass;
import ro.tuc.heroku.dtos.PersonDTO;
import ro.tuc.heroku.dtos.builders.components.LinkBuilder;
import ro.tuc.heroku.dtos.components.common.*;
import ro.tuc.heroku.entities.Person;

import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class PersonBuilder {

    public PersonDTO fhirToDTO(org.hl7.fhir.r4.model.Person person) {
        return PersonDTO.builder()
                .id(person.getId())
                .identifier(person.getIdentifier()
                        .stream()
                        .map(UtilBuilder::toIdentifier)
                        .collect(Collectors.toList()))
                .name(person.getName()
                        .stream()
                        .map(UtilBuilder::toHumanName)
                        .collect(Collectors.toList()))
                .telecom(person.getTelecom()
                        .stream()
                        .map(UtilBuilder::toContactPoint)
                        .collect(Collectors.toList()))
                .gender(person.getGender().name())
                .birthDate(person.getBirthDate().toString())
                .address(person.getAddress()
                        .stream()
                        .map(UtilBuilder::toAddress)
                        .collect(Collectors.toList()))
                .photo(UtilBuilder.toAttachment(person.getPhoto()))
                .managingOrganization(UtilBuilder.toReference(person.getManagingOrganization()))
                .active(person.getActive())
                .link(person.getLink()
                        .stream()
                        .map((link1) ->
                                LinkBuilder.fhirToLinkDTO(link1, person.getId()))
                        .collect(Collectors.toList()))
                .build();
    }

    public PersonDTO entityToPersonDTO(Person person) {
        Gson gson = new Gson();
        return PersonDTO
                .builder()
                .id(person.getId())
                .identifier(gson.fromJson(person.getIdentifier(), new TypeToken<List<Identifier>>() {
                }.getType()))
                .name(gson.fromJson(person.getName(), new TypeToken<List<HumanName>>() {
                }.getType()))
                .telecom(gson.fromJson(person.getTelecom(), new TypeToken<List<ContactPoint>>() {
                }.getType()))
                .gender(person.getGender())
                .birthDate(person.getBirthDate())
                .address(gson.fromJson(person.getAddress(), new TypeToken<List<Address>>() {
                }.getType()))
                .photo(gson.fromJson(person.getPhoto(), Attachment.class))
                .managingOrganization(gson.fromJson(person.getManagingOrganization(), Reference.class))
                .active(person.getActive())
                .link(person.getLink() == null ? null :
                        person.getLink()
                                .stream()
                                .map(LinkBuilder::entityToLinkDTO)
                                .collect(Collectors.toList()))
                .build();
    }

    public Person toEntity(PersonDTO person) {
        Gson gson = new Gson();
        return Person.builder()
                .id(person.getId())
                .identifier(gson.toJson(person.getIdentifier()))
                .name(gson.toJson(person.getName()))
                .telecom(gson.toJson(person.getTelecom()))
                .gender(person.getGender())
                .birthDate(person.getBirthDate())
                .address(gson.toJson(person.getAddress()))
                .photo(gson.toJson(person.getPhoto()))
                .managingOrganization(gson.toJson(person.getManagingOrganization()))
                .active(person.getActive())
/*                .link(person.getLink() == null ? null :
                        person.getLink()
                        .stream()
                        .map(LinkBuilder::toEntity)
                        .collect(Collectors.toList()))*/
                .build();
    }
}
