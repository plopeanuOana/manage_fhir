package ro.tuc.heroku.dtos.builders.components;

import com.google.gson.Gson;
import lombok.experimental.UtilityClass;
import org.hl7.fhir.r4.model.Person;
import ro.tuc.heroku.dtos.PersonDTO;
import ro.tuc.heroku.dtos.builders.UtilBuilder;
import ro.tuc.heroku.dtos.components.LinkDTO;
import ro.tuc.heroku.dtos.components.common.Reference;
import ro.tuc.heroku.entities.components.Link;

@UtilityClass
public class LinkBuilder {

    public LinkDTO fhirToLinkDTO(Person.PersonLinkComponent link, String person_id) {
        return LinkDTO
                .builder()
                .target(UtilBuilder.toReference(link.getTarget()))
                .assurance(link.getAssurance() == null ? null : link.getAssurance().name())
                .personDTO(PersonDTO.builder().id(person_id).build())
                .build();
    }

    public LinkDTO setPersonDTO(LinkDTO linkDTO, String person_id) {
        linkDTO.setPersonDTO(PersonDTO.builder().id(person_id).build());
        return linkDTO;
    }

    public Link toEntity(LinkDTO linkDTO) {
        Gson gson = new Gson();
        return Link
                .builder()
                .id(linkDTO.getId())
                .target(gson.toJson(linkDTO.getTarget()))
                .assurance(linkDTO.getAssurance())
                .person(ro.tuc.heroku.entities.Person.builder().id(linkDTO.getPersonDTO().getId()).build())
                .build();
    }

    public LinkDTO entityToLinkDTO(Link link) {
        Gson gson = new Gson();
        return LinkDTO
                .builder()
                .id(link.getId())
                .target(gson.fromJson(link.getTarget(), Reference.class))
                .assurance(link.getAssurance())
                .personDTO(PersonDTO.builder().id(link.getPerson().getId()).build())
                .build();
    }
}
