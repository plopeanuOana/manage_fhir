package ro.tuc.heroku.dtos.builders;

import lombok.experimental.UtilityClass;
import org.hl7.fhir.r4.model.PrimitiveType;
import ro.tuc.heroku.dtos.components.common.*;

import java.util.Arrays;
import java.util.stream.Collectors;

@UtilityClass
public class UtilBuilder {
    public static Identifier toIdentifier(org.hl7.fhir.r4.model.Identifier identifier) {
        return Identifier
                .builder()
                .use(identifier.getUse().toCode())
                .type(identifier.getType() == null ? null : toCodeableConcept(identifier.getType()))
                .system(identifier.getSystem())
                .value(identifier.getValue())
                .period(toPeriod(identifier.getPeriod()))
                .assigner(Assigner.builder()
                        .display(identifier.getAssigner() == null ? null : identifier.getAssigner().getDisplay())
                        .reference(identifier.getAssigner() == null ? null : identifier.getAssigner().getReference())
                        .type(identifier.getAssigner() == null ? null : identifier.getAssigner().getType())
                        .build())
                .build();
    }

    public HumanName toHumanName(org.hl7.fhir.r4.model.HumanName name) {
        return HumanName.builder()
                .use(name.getUse() == null ? null :
                        name.getUse().name())
                .text(name.getText())
                .family(name.getFamily())
                .given(name.getGiven()
                        .stream()
                        .map(PrimitiveType::getValue)
                        .collect(Collectors.toList()))
                .prefix(name.getPrefix()
                        .stream()
                        .map(PrimitiveType::getValue)
                        .collect(Collectors.toList()))
                .suffix(name.getSuffix()
                        .stream()
                        .map(PrimitiveType::getValue)
                        .collect(Collectors.toList()))
                .period(toPeriod(name.getPeriod()))
                .build();
    }

    public ContactPoint toContactPoint(org.hl7.fhir.r4.model.ContactPoint contactPoint) {
        return ContactPoint.builder()
                .system(contactPoint.getSystem() == null ? null : contactPoint.getSystem().name())
                .value(contactPoint.getValue())
                .use(contactPoint.getUse() == null ? null :
                        contactPoint.getUse().name())
                .rank(contactPoint.getRank() == 1)
                .period(toPeriod(contactPoint.getPeriod()))
                .build();
    }

    public Address toAddress(org.hl7.fhir.r4.model.Address address) {
        return Address.builder()
                .use(address.getUse() == null ? null : address.getUse().name())
                .type(address.getType() == null ? null : address.getType().name())
                .text(address.getText())
                .line(address.getLine()
                        .stream()
                        .map(PrimitiveType::getValue)
                        .collect(Collectors.toList()))
                .city(address.getCity())
                .district(address.getDistrict())
                .state(address.getState())
                .postalCode(address.getPostalCode())
                .country(address.getCountry())
                .period(toPeriod(address.getPeriod()))
                .build();
    }

    public Attachment toAttachment(org.hl7.fhir.r4.model.Attachment attachment) {
        return Attachment.builder()
                .contentType(attachment.getContentType())
                .language(attachment.getLanguage())
                .data(Arrays.toString(attachment.getData()))
                .url(attachment.getUrl())
                .size(attachment.getSize())
                .hash(Arrays.toString(attachment.getHash()))
                .title(attachment.getTitle())
                .creation(attachment.getCreation() == null ? null : attachment.getCreation().toString())
                .build();
    }

    public static CodeableConcept toCodeableConcept(org.hl7.fhir.r4.model.CodeableConcept codeableConcept) {
        return CodeableConcept
                .builder()
                .coding(codeableConcept.getCoding()
                        .stream()
                        .map((code) ->
                                Coding.builder()
                                        .system(code.getSystem())
                                        .code(code.getCode())
                                        .build())
                        .collect(Collectors.toList()))
                .text(codeableConcept.getText())
                .build();
    }

    public Reference toReference(org.hl7.fhir.r4.model.Reference reference) {
        return Reference.builder()
                .reference(reference.getReference())
                .type(reference.getType())
                .display(reference.getDisplay())
                .build();
    }

    private static Period toPeriod(org.hl7.fhir.r4.model.Period period) {
        return Period.builder()
                .start(period.getStart() == null ? null : period.getStart().toString())
                .end(period.getEnd() == null ? null : period.getEnd().toString())
                .build();
    }
}
