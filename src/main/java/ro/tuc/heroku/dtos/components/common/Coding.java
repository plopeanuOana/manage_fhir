package ro.tuc.heroku.dtos.components.common;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString

public class Coding {
    private final String system;
    private final String code;
}
