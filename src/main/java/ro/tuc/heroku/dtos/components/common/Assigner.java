package ro.tuc.heroku.dtos.components.common;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString

public class Assigner {
    private final String display;
    private final String reference;
    private final String type;
}
