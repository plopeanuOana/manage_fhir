package ro.tuc.heroku.dtos.components;

import lombok.*;
import ro.tuc.heroku.dtos.OrganizationDTO;
import ro.tuc.heroku.dtos.components.common.Address;
import ro.tuc.heroku.dtos.components.common.CodeableConcept;
import ro.tuc.heroku.dtos.components.common.ContactPoint;
import ro.tuc.heroku.dtos.components.common.HumanName;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ContactDTO {
    private String id;
    private CodeableConcept purpose;
    private HumanName name;
    private List<ContactPoint> telecom;
    private Address address;
    private OrganizationDTO organizationDTO;
}
