package ro.tuc.heroku.dtos.components.common;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString

public class Reference {
    private final String reference;
    private final String type;
    private final String display;
}
