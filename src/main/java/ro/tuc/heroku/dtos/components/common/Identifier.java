package ro.tuc.heroku.dtos.components.common;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class Identifier {
    private final String use;
    private final CodeableConcept type;
    private final String system;
    private final String value;
    private final Period period;
    private final Assigner assigner;
}
