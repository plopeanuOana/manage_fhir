package ro.tuc.heroku.dtos.components.common;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ContactPoint {
    private final String system;
    private final String value;
    private final String use;
    private final Boolean rank; //1=highest
    private final Period period;
}
