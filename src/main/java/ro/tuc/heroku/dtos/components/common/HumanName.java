package ro.tuc.heroku.dtos.components.common;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class HumanName {
    private final String use;
    private final String text;
    private final String family;
    private final List<String> given;
    private final List<String> prefix;
    private final List<String> suffix;
    private final Period period;
}
