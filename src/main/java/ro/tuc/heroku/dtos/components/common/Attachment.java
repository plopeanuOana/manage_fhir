package ro.tuc.heroku.dtos.components.common;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Attachment {
    private final String contentType;
    private final String language;
    private final String data;
    private final String url;
    private final int size;
    private final String hash;
    private final String title;
    private final String creation;
}
