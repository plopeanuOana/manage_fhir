package ro.tuc.heroku.dtos.components;

import lombok.*;
import ro.tuc.heroku.dtos.PersonDTO;
import ro.tuc.heroku.dtos.components.common.Reference;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class LinkDTO {
    private String id;
    private Reference target;
    private String assurance;
    private PersonDTO personDTO;
}
