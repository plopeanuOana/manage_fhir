package ro.tuc.heroku.dtos.components.common;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@Builder
@ToString

public class CodeableConcept {
    private final List<Coding> coding;
    private final String text;
}
