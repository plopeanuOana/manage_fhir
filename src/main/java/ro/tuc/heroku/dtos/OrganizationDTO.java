package ro.tuc.heroku.dtos;

import lombok.*;
import ro.tuc.heroku.dtos.components.ContactDTO;
import ro.tuc.heroku.dtos.components.common.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class OrganizationDTO {
    private String id;
    private List<Identifier> identifier;
    private Boolean active;
    private List<CodeableConcept> type;
    private String name;
    private List<String> alias;
    private List<ContactPoint> telecom;
    private List<Address> address;
    private Reference partOf;
    private List<ContactDTO> contact;
    private List<Reference> endpoint;
}
